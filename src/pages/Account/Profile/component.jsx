import React from "react";
import { View } from "react-native";
import { Button, Text } from "react-native-paper";

const Profile = ({ navigation }) => {
  return (
    <View>
      <Text>Nothing show on Profile</Text>
      <Button onPress={() => navigation.navigate("Product")} mode="contained">
        Go to Product Management
      </Button>
    </View>
  );
};

export default Profile;
