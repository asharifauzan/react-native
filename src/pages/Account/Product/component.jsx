import React, { useCallback, useContext, useState } from "react";
import { View, StyleSheet } from "react-native";
import {
  Button,
  DataTable,
  IconButton,
  Text,
  TextInput,
} from "react-native-paper";
import { ProductContext } from "../../../providers/ProductProvider";

const Product = ({ navigation }) => {
  const [input, setInput] = useState("");
  const [products, setProducts] = useContext(ProductContext);

  /* ACTION: add new product to list */
  const addProduct = async () => {
    const product_name = input;

    if (!!product_name) {
      const new_products = [
        ...products,
        {
          id: Math.random(),
          name: product_name,
        },
      ];
      setProducts(new_products);
      setInput("");
    }
  };

  /* ACTION: delete item from list */
  const deleteProduct = (id) => {
    const new_products = products.filter((product) => product.id !== id);
    setProducts(new_products);
  };

  /* ACTION: clear all products */
  const clearProduct = () => {
    setProducts([]);
  };

  const changeInput = (text) => {
    setInput(text);
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
    },
  });

  const renderList = useCallback(() => {
    if (!products.length) return <Text>Tidak ada data</Text>;

    return (
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>ID Product</DataTable.Title>
          <DataTable.Title>Nama Product</DataTable.Title>
          <DataTable.Title>Aksi</DataTable.Title>
        </DataTable.Header>
        {products.map((product) => (
          <DataTable.Row key={product.id}>
            <DataTable.Cell>{product.id}</DataTable.Cell>
            <DataTable.Cell>{product.name}</DataTable.Cell>
            <DataTable.Cell>
              <IconButton
                icon="eye"
                mode="contained"
                onPress={() =>
                  navigation.navigate("Product-View", { data: product })
                }
              />
              <IconButton
                icon="pencil"
                mode="contained"
                onPress={() =>
                  navigation.navigate("Product-Edit", { data: product })
                }
              />
              <IconButton
                icon="trash-can"
                mode="contained"
                onPress={() => deleteProduct(product.id)}
              />
            </DataTable.Cell>
          </DataTable.Row>
        ))}
      </DataTable>
    );
  }, [products]);

  return (
    <View p={8}>
      <TextInput
        placeholder="Enter product name here"
        value={input}
        onChangeText={changeInput}
      />
      <Button onPress={() => addProduct()} mode="outlined">
        <Text>Tambah data</Text>
      </Button>
      {renderList()}
      <Button onPress={clearProduct} mode="contained">
        Clear
      </Button>
    </View>
  );
};

export default Product;
