import { useContext, useState } from "react";
import { View } from "react-native";
import { Button, TextInput } from "react-native-paper";
import { ProductContext } from "../../../providers/ProductProvider";

const ProductEdit = ({ navigation, route }) => {
  const [data, setData] = useState(route.params.data);
  const [products, setProducts] = useContext(ProductContext);

  const handleChange = (newObject) => {
    setData((prevData) => ({ ...prevData, ...newObject }));
  };

  const handleSubmit = () => {
    const newProducts = products.map((old) => {
      if (old.id === data.id) {
        old.name = data.name;
      }
      return old;
    });

    setProducts(newProducts);

    navigation.goBack();
  };

  return (
    <View>
      <TextInput
        size="md"
        value={data.id.toString()}
        keyboardType="numeric"
        editable={false}
      />
      <TextInput
        size="md"
        value={data.name}
        onChangeText={(text) => handleChange({ name: text })}
      />
      <Button onPress={handleSubmit} mode="contained">
        Edit
      </Button>
    </View>
  );
};

export default ProductEdit;
