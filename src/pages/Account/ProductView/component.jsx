import React from "react";
import { View } from "react-native";
import { Text } from "react-native-paper";

const ProductView = ({ route }) => {
  const { id, name } = route.params.data;
  return (
    <View>
      <Text>{id}</Text>
      <Text>{name}</Text>
    </View>
  );
};

export default ProductView;
