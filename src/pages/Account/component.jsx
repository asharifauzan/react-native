import React from "react";
import { SafeAreaView } from "react-native";
import { Text } from "react-native-paper";

const Account = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Text>Hello world</Text>
    </SafeAreaView>
  );
};

export default Account;
