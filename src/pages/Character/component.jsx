import React, { useCallback, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import { DataTable, Text } from "react-native-paper";

const ENDPOINT = "https://gsi.fly.dev/characters?limit=99999";

const Character = () => {
  const [loading, setLoading] = useState(false);
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    setLoading(true);
    (async () => {
      const response = await fetch(ENDPOINT);
      const data = await response.json();
      setCharacters(data.results);
      setLoading(false);
    })();
  }, []);

  const renderList = useCallback(() => {
    return (
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Character Name</DataTable.Title>
          <DataTable.Title>Rarity</DataTable.Title>
        </DataTable.Header>
        {characters.map((character) => (
          <DataTable.Row key={character.id}>
            <DataTable.Cell>{character.name}</DataTable.Cell>
            <DataTable.Cell>{character.rarity}</DataTable.Cell>
          </DataTable.Row>
        ))}
      </DataTable>
    );
  }, [characters]);

  if (loading) return <Text>Loading....</Text>;

  return <ScrollView>{renderList()}</ScrollView>;
};

export default Character;
