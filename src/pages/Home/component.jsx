import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Text } from "react-native-paper";
import FABComponent from "../../components/FABComponent";

const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text variant="displaySmall">Welcome To Genshin Impact Gallery</Text>
      <Button onPress={() => navigation.navigate("Account")} mode="contained">
        Go to Profile
      </Button>
      <Button>Klik</Button>
      <FABComponent />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
});

export default Home;
