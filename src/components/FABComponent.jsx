import React, { useContext, useState } from "react";
import { NavigationContext } from "@react-navigation/native";
import { Portal, FAB } from "react-native-paper";

const FABComponent = () => {
  const navigationContext = useContext(NavigationContext);
  const [open, setOpen] = useState(false);

  return (
    <Portal>
      <FAB.Group
        open={open}
        visible
        icon={open ? "chevron-down" : "chevron-up"}
        actions={[
          {
            icon: "package-variant",
            label: "Product Management",
            onPress: () => {
              navigationContext.navigate("Product");
            },
          },
          {
            icon: "account",
            label: "My Profile",
            onPress: () => {
              navigationContext.navigate("Profile");
            },
          },
        ]}
        onStateChange={() => setOpen((prevState) => !prevState)}
        style={{ bottom: 50 }} // avoid overlay bottom tab
      />
    </Portal>
  );
};
export default FABComponent;
