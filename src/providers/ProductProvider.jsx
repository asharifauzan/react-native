import { useAsyncStorage } from "@react-native-async-storage/async-storage";
import React, { createContext, useEffect, useState } from "react";

export const ProductContext = createContext();

const STORAGE_KEY = "@product";

const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const { getItem, setItem } = useAsyncStorage(STORAGE_KEY);

  /* ACTION: load current item list */
  const loadInitialItems = async () => {
    const parsedProducts = await getItem();

    if (parsedProducts) {
      setProducts(JSON.parse(parsedProducts)); // array of objects
      return;
    }

    setProducts([]); // empty array if not exists
  };

  /* ACTION: save current item */
  const setSubscribeCurrentItems = async () => {
    const stringifiedProducts = JSON.stringify(products);
    await setItem(stringifiedProducts);
  };

  useEffect(() => {
    loadInitialItems();
  }, []);

  useEffect(() => {
    setSubscribeCurrentItems();
  }, [products]);

  return (
    <ProductContext.Provider value={[products, setProducts]}>
      {children}
    </ProductContext.Provider>
  );
};

export default ProductProvider;
