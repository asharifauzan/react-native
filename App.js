import React from "react";
import { IconButton, PaperProvider, Button } from "react-native-paper";
import { NavigationContainer } from "@react-navigation/native";
import ProductProvider from "./src/providers/ProductProvider";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { DefaultTheme } from "react-native-paper";

// Bottom Tab Screen
import Home from "./src/pages/Home/component";
import Character from "./src/pages/Character/component";
import Account from "./src/pages/Account/component";

// Stack Screen
import Profile from "./src/pages/Account/Profile/component";
import Product from "./src/pages/Account/Product/component";
import ProductView from "./src/pages/Account/ProductView/component";
import ProductEdit from "./src/pages/Account/ProductEdit/component";

function BottomComponent() {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator initialRouteName="Home">
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: () => <IconButton icon="home" />,
        }}
      />
      <Tab.Screen
        name="Character"
        component={Character}
        options={{
          tabBarIcon: () => <IconButton icon="gamepad-variant" />,
        }}
      />
      <Tab.Screen
        name="Account"
        component={Account}
        options={{
          tabBarIcon: () => <IconButton icon="account" />,
        }}
      />
    </Tab.Navigator>
  );
}

export default function App() {
  const Stack = createStackNavigator();

  return (
    <PaperProvider theme={DefaultTheme}>
      <ProductProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Front">
            <Stack.Screen
              name="Front"
              component={BottomComponent}
              options={{ headerShown: false }}
            />
            <Stack.Screen name="Profile" component={Profile} />
            <Stack.Screen name="Product" component={Product} />
            <Stack.Screen name="Product-View" component={ProductView} />
            <Stack.Screen name="Product-Edit" component={ProductEdit} />
          </Stack.Navigator>
        </NavigationContainer>
      </ProductProvider>
    </PaperProvider>
  );
}
